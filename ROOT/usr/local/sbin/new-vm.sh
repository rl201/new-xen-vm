#!/bin/bash

# to create virtual machines on LVM or DRBD
SSH="ssh twin"
SCP='scp'
XM='xl'
ME=`hostname`
set -e 

### Functions

# Script:
function usage() {
 cat <<EOF
 Usage: $0 -h Hostname (FQDN) [-o] [-r] [-s size/GB] [-v volume_group] [-M memory/MB ] [-d DIST] [-w] [-S] [-p DRBD_PRIORITY]
 builds a virtual machine. 
 '?' this help
 -h fully-qualified hostname (NB local part should be <=15 chars if you plan to winbind)
 -o over-writes any existing config files if the machine is not already running
 -r reinstall existing VM rather than setting up a new one
 -s disk size in GB (default: 10)
 -v volume group name
 -M RAM for the VM (default: 512)
 -d jessie/xenial/bionic/focal . default=bionic
 -S standalone ie there is no twin machine
 -w Configure as a workstation (rather than server)
 -p set sync-after priority for DRBD (default: 10)
EOF
 exit 1
}


### Functions which ensure the world is ready for this machine

 ##
 # Check whether a named DRBD device exists
 ##
function drbdexists() {
 local SHORTHOST=$1
 ((drbdadm state $SHORTHOST >/dev/null 2>&1) &&
  (! drbdadm state $SHORTHOST 2>&1 | grep -q "no resources defined" )) 
}

 ##
 # Set MAC address from database, bale if not found 
 ##
function ensuremac() {
 local NEWHOST=$1
 MACADDR=$( wget -q -O - http://database.maths.cam.ac.uk/api/mac_addrs | jq '. | select(.valid == true) | .data[] | select (.fqdn== "'$NEWHOST'") | .mac' -r)
 #MACADDR=` wget -q -O - http://linux-builder.ch.cam.ac.uk/fai/getmac.php?hostname=$NEWHOST`
 if [ -z $MACADDR ] ; then
  echo No MAC address could be found.
  usage
  exit 1
 fi
}

## ensure that our MAC address is not already in use
function checkuniquemac() {
 # .maths ensures this by a DB constraint, I hope.
 #local MACADDR=$1
 #JSON=$(wget -q -O - "http://linux-builder.ch.cam.ac.uk/fai/checkuniquemac.php?mac=$MACADDR")
 #ISUNIQUE=$(echo $JSON | jq '.isunique')
 #if [ "$ISUNIQUE" == 0 ] ; then
  #echo Error: $MACADDR is not unique - used by..
  #echo $JSON | jq '.hostinfo' | jq 'keys'
  #exit 1
 #fi
 echo OK
}

 ##
 # Check we can resolve this hostname (i.e. there's an IP address set)
 # and bale if not.
 ##
function ensuredns() {
 NEWHOST=$1
 # Is hostname in DNS yet?
 if ! getent hosts $NEWHOST >/dev/null 2>&1 ; then
 cat <<EOF
 * Cannot resolve $NEWHOST. Not proceeding further. 
 * (you could always add to /etc/hosts if required)
EOF
 exit 3
fi
}

 ## 
 # Check we haven't already created a DRBD device with this name
 # Bale out if we have
 ## 
function ensurenodrbd() {
 local SHORTHOST=$1
 if drbdexists $SHORTHOST ; then
  cat <<EOF
  * The short name ($SHORTHOST) is already in use, according to DRBD.
  * Stop the drbd, edit /etc/drbd.conf and remove references to it if
  * you're sure that it's not in use.
umount -f `drbdadm sh-dev $SHORTHOST`
drbdadm down $SHORTHOST && rm /etc/drbd.d/$SHORTHOST.res
$SSH drbdadm down $SHORTHOST && $SSH rm /etc/drbd.d/$SHORTHOST.res
EOF
  exit 5
 fi
 if (grep ^resource /etc/drbd.conf | grep -q '[ "]'$SHORTHOST'[ "]' ) ; then
  cat <<EOF
  * $SHORTHOST is still listed in /etc/drbd.conf. You need to remove that before continuing.
EOF
  exit 5
 fi
 if ([ -d /etc/drbd.d/ ] && [ -f /etc/drbd.d/${SHORTHOST}.res ]) ||
    ($SSH [ -d /etc/drbd.d/ ] && $SSH [ -f /etc/drbd.d/${SHORTHOST}.res ]) ; then
  if ($OVERWRITE) ; then
  cat <<EOF
   * Removing configuration at /etc/drbd.d/${SHORTHOST}.res.
EOF
  rm -f /etc/drbd.d/${SHORTHOST}.res 
   $SSH rm -f /etc/drbd.d/${SHORTHOST}.res
  fi;
  if [ -d /etc/drbd.d/ ] && [ -f /etc/drbd.d/${SHORTHOST}.res ] ; then
   cat <<EOF
   * Failed to remove configuration at /etc/drbd.d/${SHORTHOST}.res. 
EOF
   exit 5
  fi
 fi
 
}

 ##
 # Check we haven't already created a DomU with this name, bale if we have
 ##
function ensurenovm() {
 local NEWHOST=$1
 # Does that VM exist?
 if $XM list $NEWHOST >/dev/null 2>&1 ; then
  cat <<EOF
  * Xen is already running a machine of that name. If you want to replace that VM, 
  * stop the virtual machine first, and remove entries from /etc/drbd.conf, /etc/ha.d/haresources
EOF
  exit 4
 fi
}

 ##
 # Over-long names are bad for Windows domains. Check this.
 ##
function warnifnametoolong() {
 local SHORTHOST=$1
 if [ ${#SHORTHOST} -gt 15 ] ; then
  cat <<EOF
  * ${SHORTHOST} is ${#SHORTHOST} characters long. Names longer than 15 characters
  * cannot be joined to the Active Directory, so you may want to choose something
  * shorter if you plan to winbind this VM.
EOF
 fi
}

function getvlan() {
  # .maths hasn't got as far as VLANs yet
  #VLAN=$(wget -O - http://linux-builder.ch.cam.ac.uk/ansible/vlan.php?mac=$MACADDR |head -1) 
  #if [ -z $VLAN ]
  #then
    #echo No VLAN found, using vlan 1
    #VLAN=1
  #fi
  echo OK
}

### Functions which adjust the Dom0 to handle this machine

 ##
 # To check that we have a few tools which might not be here
 ##
function checkbinaries() {
 debootstrap --version
 pvdisplay --version
}

 ##
 # Boolean function to check whether DRBD is in use
 ##
function checkdrbd() {
 if [ -f /proc/drbd ] && [ "x$STANDALONE" == 'x' ]; then
  DRBDINUSE=true
 else
  DRBDINUSE=false
 fi
}

 ## 
 # Configure heartbeat
 ##
function configha() {
 # Add this machine to heartbeat
 local HA_STRING="xenlive::$SHORTHOST"
 local HA_FILE="/etc/ha.d/haresources"
 grep -qxF "$HA_STRING" $HA_FILE || echo "$HA_STRING" >> $HA_FILE
 $SCP $HA_FILE root@twin:$HA_FILE

 # Copy the config to twin
 $SCP /etc/xen/${SHORTHOST}.cfg root@twin:/etc/xen/${SHORTHOST}.cfg
}
 
 ##
 # Creates a DRBD device, set BLOCKDEV
 ##
function createdrbd() {
 local SHORTHOST=$1
 if [ -d /etc/drbd.d/ ] ; then
  newdrbd $SHORTHOST $VGROUP >>/etc/drbd.d/${SHORTHOST}.res
  # Ugly hack for .maths testing
  if [ -x /etc/drbd.d/make-drbds-sync-after.sh ] ; then
   /etc/drbd.d/make-drbds-sync-after.sh
  else
   /usr/local/sbin/make-drbds-sync-after.sh
  fi
  drbdadm adjust all || true
  $SCP /etc/drbd.d/*.res root@twin:/etc/drbd.d/
  $SSH drbdadm adjust all || true
 else 
  cp /etc/drbd.conf /etc/drbd.conf-pre-$SHORTHOST
  $SSH cp /etc/drbd.conf /etc/drbd.conf-pre-$SHORTHOST
  newdrbd $SHORTHOST $VGROUP >>/etc/drbd.conf
  $SCP /etc/drbd.conf root@twin:/etc/drbd.conf
 fi
 # Start the drbd
 yes | drbdadm create-md $SHORTHOST
 yes | $SSH drbdadm create-md $SHORTHOST
 drbdadm up $SHORTHOST
 $SSH drbdadm up $SHORTHOST
 DEVICE=`drbdadm dump $SHORTHOST | grep -A2 $ME | grep device | awk ' { print $2 ; } ' | sed s/\;//`
 drbdsetup $DEVICE primary --overwrite-data-of-peer
 sleep 6
 drbdadm secondary $SHORTHOST
 BLOCKDEV=`drbdadm sh-dev $SHORTHOST`
}

function getblockdev() {
 if $DRBDINUSE ; then
  drbdadm primary $SHORTHOST
  BLOCKDEV=`drbdadm sh-dev $SHORTHOST`
 else
  BLOCKDEV=/dev/${VGROUP}/${LVNAME}
 fi
}

 ##
 # Create logical volume on this node, and on twin IFF drbd is in use 
 ##
function createlv() {
 local SHORTHOST=$1
 local VGROUP=$2
 local DISKSIZE=$3
 ensurenolv $SHORTHOST
 # Create the logical volume
 lvcreate --size $DISKSIZE --name $SHORTHOST $VGROUP
 if $DRBDINUSE; then
  $SSH lvcreate --size $DISKSIZE --name $SHORTHOST $VGROUP
 fi
 # And blank them
 MDSIZE=500000
 SIZE=`lvs --units b /dev/${VGROUP}/${LVNAME} | grep $SHORTHOST | awk ' { print \$4 ; } ' | sed s/B//`
 SKIP=$((SIZE-MDSIZE))
 dd if=/dev/zero of=/dev/${VGROUP}/${LVNAME} bs=1 count=$MDSIZE skip=$SKIP
 dd if=/dev/zero of=/dev/${VGROUP}/${LVNAME} bs=1 count=$MDSIZE seek=$SKIP
 if $DRBDINUSE ; then
  $SSH dd if=/dev/zero of=/dev/${VGROUP}/${LVNAME} bs=1 count=$MDSIZE skip=$SKIP
  $SSH dd if=/dev/zero of=/dev/${VGROUP}/${LVNAME} bs=1 count=$MDSIZE seek=$SKIP
 fi
}

 ##
 # We haven't already got an LV of that name, have we?
 ##
function ensurenolv() {
 local SHORTHOST=$1
 # Does that VG exist?
 # (That grep call is needed - vgdisplay of a missing vg returns 0)
 if ! vgdisplay $VGROUP 2>&1 | grep -q Metadata ; then
  cat <<EOF
  * That volume group does not exist. Please choose from:
EOF
  vgdisplay | grep Name | awk ' { print $3 ; } '
  exit 6
 fi
 # Does that LV exist?
 LVNAME=${SHORTHOST}
 if [ -b /dev/${VGROUP}/${LVNAME} ]; then
  if ($OVERWRITE) ; then
   if (mount | grep -q /dev/${VGROUP}/${LVNAME}) then
    if lsof /dev/${VGROUP}/${LVNAME} >/dev/null 2>&1 ; then
     lsof /dev/${VGROUP}/${LVNAME}  | awk ' { print $2 ; } ' | grep -v PID| xargs -r kill -9 
    fi
    umount -f /dev/${VGROUP}/${LVNAME} || true
   fi
   lvremove -f /dev/${VGROUP}/${LVNAME}
   if $DRBDINUSE ; then
    $SSH lvremove -f /dev/${VGROUP}/${LVNAME}
   fi
  else
   cat <<EOF
  * That logical volume already exists. Perhaps you need to remove it with 
  * lvremove /dev/${VGROUP}/${LVNAME}
EOF
   exit 7
  fi
 fi
}

 ##
 # Creates a new DRBD device
 ##
function newdrbd() {
 local NAME=$1
 # //FOO/BAR replaces all FOO with BAR, not just 1st FOO
 LVNAME=${NAME}
 VG=$2
 local VGROUP=$VG
 THISHOST=`hostname`
 TWINHOST=`$SSH hostname`
 LASTDEV=`cat /proc/drbd | grep '[ds:|ro:]' | awk ' { print $1 ; } ' | sed s/:// | sort -n | tail -1`
 THISDEV=drbd$((LASTDEV+1))
 LASTDEV=`$SSH cat /proc/drbd | grep '[ds:|ro:]' | awk ' { print $1 ; } ' | sed s/:// | sort -n | tail -1`
 TWINDEV=drbd$((LASTDEV+1))

 #TWINIP=`$SSH ip address show dev $REPNET_INTERFACE | grep 'inet ' | awk ' { print $2 ; } ' | cut -d/ -f1`
 TWINIP=$(getent hosts twin | awk ' { print $1 } ')
 REPNET_INTERFACE=$(ip route get $TWINIP | grep "$TWINIP" | sed 's/.* dev //' | awk ' { print $1 } ')
 THISIP=`ip address show dev $REPNET_INTERFACE | grep 'inet ' | awk ' { print $2 ; } '| cut -d/ -f1`
 # if this is the first drbd need to start port numbers at 7789 or something
 LASTPORT=`drbdadm dump | grep $THISIP | sed s/.*:// | sed s/\;// | sort -n | tail -1`
 if [ -z "$LASTPORT" ] ; then LASTPORT=7788; fi
 THISPORT=$((LASTPORT+1))
 LASTPORT=`$SSH drbdadm dump | grep $TWINIP | sed s/.*:// | sed s/\;// | sort -n | tail -1`
 if [ -z "$LASTPORT" ] ; then TWINPORT=7788; fi
 TWINPORT=$((LASTPORT+1))
 
cat <<EOF
# Automatically generated by $0 on `hostname` at `date`
resource "$NAME" {
EOF

if [ -n "$LASTDEV" ] ; then
cat <<EOF
 syncer {
 # Priority $DRBD_PRIORITY
 after foo;
 }
EOF
fi
cat <<EOF
 on $THISHOST {
  device        /dev/$THISDEV;
  disk          /dev/${VGROUP}/${LVNAME};
  address       $THISIP:$THISPORT;
  meta-disk     internal;
 }
 on $TWINHOST {
  device        /dev/$TWINDEV;
  disk          /dev/${VGROUP}/${LVNAME};
  address       $TWINIP:$TWINPORT;
  meta-disk     internal;
 }
}

EOF
}

 ##
 # Writes config for Xen
 ##
function writexenconfig() {
 local DISTRO=$1
 if $DRBDINUSE ; then
  DISK="['drbd:$SHORTHOST,xvda1,w']"
 else
  if [ "$XM" == "xl" ] ; then
   DISK="['$BLOCKDEV,raw,xvda1,w']"
  else
   DISK="['phy:$BLOCKDEV,xvda1,w']"
  fi
 fi
 cat >/etc/xen/$SHORTHOST.cfg <<EOF
# Automatically created by $0 at `date`

#kernel      = '/etc/xen/kernels/${DISTRO}/vmlinuz'
#ramdisk     = '/etc/xen/kernels/${DISTRO}/initrd.img'
#extra       = 'clocksource=xen xencons=tty console=hvc0 elevator=noop'
#root        = '/dev/xvda1 ro'

type="pvh"
kernel="/usr/lib/grub-xen/grub-i386-xen_pvh.bin"

memory = '$MEMORY'
 
# Disks
disk = $DISK

# Hostname
name='$SHORTHOST'

# Networking - ensure that the MAC address is unique.
vif=['bridge=xenbr0, mac=$MACADDR']

on_poweroff = 'destroy'
on_reboot   = 'restart'
on_crash    = 'restart'

vncpasswd = ''

cpus = 'all,^0-1'

vcpus = 1
EOF
}

function unmountchroot() {
 local DIR=$1
 sleep 2
 umount $DIR ||true
}

### Functions which operate directly in the DomU

 ##
 # Set the root password 
 ##
function setrootpw() {
 local DIR=$1
 echo "root:$PASSWD" | chroot $DIR chpasswd
}

function getrootpasswd() {
 echo "******************************************************"
 echo "  Type the root password for the new VM and hit enter:"
 read -e PASSWD
 echo The root password for your VM will be:
 echo $PASSWD
 echo Hit enter if that\'s OK, or ctrl-c to start again
 read
 if [ -z $PASSWD ] ; then
  echo The root password cannot be blank
  exit 1
 fi
}

function gettyonconsole() {
 local DIR=$1
 echo '7:2345:respawn:/sbin/getty 38400 hvc0' >> $DIR/etc/inittab
}

function sourceslist() {
 local DIR=$1
 local DISTRO=$2

 # NB deliberately do this right before adding sources: our local repo
 # is now https so if we add that before we have apt-transport-https
 # we are not going to get very much further!
 DEBIAN_FRONTEND=noninteractive chroot $DIR apt-get install -y apt-transport-https

 if [ "$DISTRO" == "jessie" ];then

  cat <<EOF > $DIR/etc/apt/sources.list
#deb http://ftp.us.debian.org/debian jessie main non-free contrib
deb http://security.debian.org/ jessie/updates main contrib non-free
deb http://www-uxsup.csx.cam.ac.uk/pub/linux/debian jessie main non-free contrib
deb-src http://www-uxsup.csx.cam.ac.uk/pub/linux/debian jessie main non-free contrib
EOF

 elif [ "$DISTRO" == "xenial" ];then

 cat <<EOF > $DIR/etc/apt/sources.list
deb http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu xenial main restricted universe multiverse
deb-src http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu xenial main restricted universe multiverse

# NB we deliberately include the DIST-updates and DIST-security distributions, otherwise
# 1) we debootstrap the VM
# 2) we run ansible which adds both of these at an early stage
# 3) there are immediately a pile of updates that need applying
deb http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu xenial-updates main restricted universe multiverse
deb-src http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu xenial-updates main restricted universe multiverse
deb http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu xenial-security main restricted universe multiverse
deb-src http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu xenial-security main restricted universe multiverse

EOF

 cat <<EOF >$DIR/etc/apt/sources.list.d/chemistry.list
deb https://downloads.ch.cam.ac.uk/local-debs xenial ucamchem
EOF

 elif [ "$DISTRO" == "bionic" ];then

 cat <<EOF > $DIR/etc/apt/sources.list
deb http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu bionic main restricted universe multiverse
deb-src http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu bionic main restricted universe multiverse

# NB we deliberately include the DIST-updates and DIST-security distributions, otherwise
# 1) we debootstrap the VM
# 2) we run ansible which adds both of these at an early stage
# 3) there are immediately a pile of updates that need applying
deb http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu bionic-updates main restricted universe multiverse
deb-src http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu bionic-updates main restricted universe multiverse
deb http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu bionic-security main restricted universe multiverse
deb-src http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu bionic-security main restricted universe multiverse
EOF

 cat <<EOF >$DIR/etc/apt/sources.list.d/chemistry.list
deb https://downloads.ch.cam.ac.uk/local-debs bionic ucamchem
EOF
 elif [ "$DISTRO" == "focal" ];then

 cat <<EOF > $DIR/etc/apt/sources.list
deb http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu focal main restricted universe multiverse
deb-src http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu focal main restricted universe multiverse

# NB we deliberately include the DIST-updates and DIST-security distributions, otherwise
# 1) we debootstrap the VM
# 2) we run ansible which adds both of these at an early stage
# 3) there are immediately a pile of updates that need applying
deb http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu focal-updates main restricted universe multiverse
deb-src http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu focal-updates main restricted universe multiverse
deb http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu focal-security main restricted universe multiverse
deb-src http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu focal-security main restricted universe multiverse
EOF

 cat <<EOF >$DIR/etc/apt/sources.list.d/chemistry.list
deb https://downloads.ch.cam.ac.uk/local-debs focal ucamchem
EOF
 fi

chroot $DIR apt-get update
}

# install grub
function installgrub() {
 local DIR=$1
 mount -o bind /dev $DIR/dev
 mount -o bind /sys $DIR/sys
 mount -t proc proc $DIR/proc
 chroot $DIR update-grub
 umount $DIR/proc
 umount $DIR/sys
 umount $DIR/dev
}

# minimum set of packages we need to add in order to then run ansible
function installpackages() {
 local DIR=$1
 DEBIAN_FRONTEND=noninteractive chroot $DIR apt-get install -y lsb-release
 # ensure that updates available in our modified sources list entries (after
 # the intial debootstrap) are installed.
 DEBIAN_FRONTEND=noninteractive chroot $DIR apt-get -y full-upgrade
}

function installworkstationpackages() {
 local DIR=$1
 DEBIAN_FRONTEND=noninteractive chroot $DIR apt-get install -y chem-ansible-workstation environment-modules git tcl
 # ensure that updates available in our modified sources list entries (after
 # the intial debootstrap) are installed.
 DEBIAN_FRONTEND=noninteractive chroot $DIR apt-get -y full-upgrade
}

function writefstab() {
 local DIR=$1
 cat <<EOF >> $DIR/etc/fstab
/dev/xvda1 / ext4 errors=remount-ro 0       1
EOF
}


 ##
 # Bootstrap the basics of a machine into a given directory
 ##
function bootstrap() {
 local BLOCKDEV=$1
 local DISTRO=$2
 # Now, prepare the VM and boot it on the network
 # Except we can't PXE boot the damn things.
 echo Bootstrapping a basic OS onto the host
 mkdir -p /mnt/xen-$SHORTHOST
 mount $BLOCKDEV /mnt/xen-$SHORTHOST
 if [ "$DISTRO" == "jessie" ];then
  debootstrap --include=udev,openssh-server,less,sudo,vim,nano,rsync,python,python-apt,python-httplib2 $DISTRO /mnt/xen-$SHORTHOST http://www-uxsup.csx.cam.ac.uk/pub/linux/debian
 elif [ "$DISTRO" == "xenial" ];then
  # debootstrap on jessie doesn't have the xenial script yet. 
  test -e /usr/share/debootstrap/scripts/xenial || SCRIPT=/usr/share/debootstrap/scripts/trusty
  debootstrap --include=udev,openssh-server,less,sudo,vim,nano,rsync,python,python-apt,python-httplib2,man-db,aptitude $DISTRO /mnt/xen-$SHORTHOST http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu $SCRIPT
 elif [ "$DISTRO" == "bionic" ];then
  # debootstrap on xenial doesn't have the bionic script yet. 
  test -e /usr/share/debootstrap/scripts/bionic || SCRIPT=/usr/share/debootstrap/scripts/xenial
  debootstrap --include=udev,openssh-server,less,sudo,vim,nano,rsync,python,python-apt,python-httplib2,man-db,aptitude $DISTRO /mnt/xen-$SHORTHOST http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu $SCRIPT
 elif [ "$DISTRO" == "focal" ];then
  # debootstrap on buster doesn't have the focal script yet. 
  test -e /usr/share/debootstrap/scripts/focal || SCRIPT=/usr/share/debootstrap/scripts/xenial
  debootstrap --components=main,restricted --include=udev,openssh-server,grub-xen,linux-image-generic,python-is-python3 $DISTRO /mnt/xen-$SHORTHOST http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu $SCRIPT
 fi

 IP=`getent hosts $NEWHOST | awk '{print $1}'`

 # Add network and resolv.conf settings from the database
 if [ "$DISTRO" != "bionic" ] && [ "$DISTRO" != "focal" ]; then
  cat <<EOF >/mnt/xen-$SHORTHOST/etc/network/interfaces
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet static
EOF
  wget -q -O - http://linux-builder.ch.cam.ac.uk/fai/getnet.php?hostname=$NEWHOST >>/mnt/xen-$SHORTHOST/etc/network/interfaces
  wget -q -O - http://linux-builder.ch.cam.ac.uk/fai/getresolv.php?hostname=$NEWHOST >/mnt/xen-$SHORTHOST/etc/resolv.conf
 else
  #wget -q -O - http://linux-builder.ch.cam.ac.uk/fai/getnetplan.php?hostname=$NEWHOST >/mnt/xen-$SHORTHOST/etc/netplan/01-netcfg.yaml
 
# Well, isn't this a horrible hack. TODO: make this more elegant by creating the server-side infrastructure for maths.
NEWIP=$(getent hosts $NEWHOST | awk ' { print $1 } ')
ROUTER=$(echo $NEWIP | awk ' { print $1 } ' | awk -F . ' { print $1"."$2"."$3".62" } ')
cat >/mnt/xen-$SHORTHOST/etc/netplan/01-netcfg.yaml  <<EOF
network:
  version: 2
  renderer: networkd
  ethernets:
    eth0:
      addresses:
        - $NEWIP/24
      gateway4: $ROUTER
      nameservers:
          search: [damtp.cam.ac.uk, maths.cam.ac.uk, dpmms.cam.ac.uk, statslab.cam.ac.uk, newton.cam.ac.uk, cam.ac.uk]
          addresses: [131.111.16.129,131.111.16.32,131.111.16.30]
EOF
 fi

 if [ "$DISTRO" == "xenial" ];then
  wget -q -O - http://linux-builder.ch.cam.ac.uk/fai/getresolv.php?hostname=$NEWHOST | grep '^nameserver' >> /mnt/xen-$SHORTHOST/etc/resolvconf/resolv.conf.d/base
  wget -q -O - http://linux-builder.ch.cam.ac.uk/fai/getresolv.php?hostname=$NEWHOST | grep '^search' >> /mnt/xen-$SHORTHOST/etc/resolvconf/resolv.conf.d/tail
 fi

 echo $SHORTHOST >/mnt/xen-$SHORTHOST/etc/hostname
 echo $IP $SHORTHOST $NEWHOST >>/mnt/xen-$SHORTHOST/etc/hosts

 # fixup permissions on /tmp
 chmod 1777 /mnt/xen-$SHORTHOST/tmp

}

 ##
 # Put the SSH keys in place
 ##
function sshkeys() {
 # TODO: Create the server-side infrastructure to sort this out
 echo Not sorting SSH host keys out
 (cd /root && tar -cf - .ssh) | (cd /mnt/xen-$SHORTHOST/root && tar -xvf -)
 #local DIR=$1
 ## sort out SSH keys - copied from a FAI script
 #URL=http://linux-builder.ch.cam.ac.uk/fai/ssh.php
#
 #wget -O $DIR/ssh_host_rsa_key.pub $URL?file=ssh_host_rsa_key.pub
 #wget -O $DIR/ssh_host_rsa_key     $URL?file=ssh_host_rsa_key
 #wget -O $DIR/ssh_host_dsa_key.pub $URL?file=ssh_host_dsa_key.pub
 #wget -O $DIR/ssh_host_dsa_key     $URL?file=ssh_host_dsa_key
 #wget -O $DIR/ssh_host_ecdsa_key     $URL?file=ssh_host_ecdsa_key
 #wget -O $DIR/ssh_host_ecdsa_key.pub     $URL?file=ssh_host_ecdsa_key.pub
 #wget -O $DIR/ssh_host_ed25519_key     $URL?file=ssh_host_ed25519_key
 #wget -O $DIR/ssh_host_ed25519_key.pub     $URL?file=ssh_host_ed25519_key.pub
 #chmod 0600 $DIR/ssh_host_rsa_key $DIR/ssh_host_dsa_key $DIR/ssh_host_ecdsa_key $DIR/ssh_host_ed25519_key
#
 ## Add ssh authorized keys
 #mkdir -p /mnt/xen-$SHORTHOST/root/.ssh
 #wget -q -O /mnt/xen-$SHORTHOST/root/.ssh/authorized_keys http://linux-builder.ch.cam.ac.uk/fai/preseed_authorized_keys
 #chmod 0600 /mnt/xen-$SHORTHOST/root/.ssh/authorized_keys
}

function getchemgpgkey() {
 local DIR=$1
 wget -O $DIR/etc/apt/trusted.gpg.d/chemistry.gpg https://downloads.ch.cam.ac.uk/hogthrob.gpg
}

function ansibleinitial() {
 local DIR=$1
 cat <<EOF >$DIR/etc/systemd/system/ansible-initial.service
[Unit]
Description=Run ansible on this machine
After=network.target dbus.service
[Service]
ExecStart=/usr/local/sbin/ansible-initial.sh
TTYPath=/dev/tty2
TTYReset=yes
TTYVHangup=yes
User=root
[Install]
WantedBy=multi-user.target default.target
EOF

# chroot $DIR systemctl enable ansible-initial

cat <<EOF >$DIR/usr/local/sbin/ansible-initial.sh
#!/bin/bash
mkdir -p /var/log/chemistry-config

# If ansible has already run, note this
test -f /var/log/chemistry-config/ansible-initial.log && FAILED=true

(
export MODULEPATH=/etc/environment-modules/modules
source /etc/profile.d/modules.sh
module add ansible/workstation
PYTHONUNBUFFERED=1 ansible-pull --full -i hosts -U http://linux-builder.ch.cam.ac.uk/ansible-xenial --extra-vars='chem_safe=true'

# If ansible ran fine
if [ $? -eq 0 ]; then
  /bin/systemctl disable ansible-initial.service
  sleep 10
  reboot
fi
# Reboot if it has only failed once, i.e don't do infinite reboot if something is wrong
if [ -z "$FAILED" ];then
  echo "Going for reboot"
  reboot
fi
echo "Install has failed twice, not rebooting"
) &>> /var/log/chemistry-config/ansible-initial.log
EOF

 chmod +x $DIR/usr/local/sbin/ansible-initial.sh
 # this is what "systemctl enable ansible-initial" does inside the VM, so replicate manually
 mkdir -p $DIR/etc/systemd/system/multi-user.target.wants $DIR/etc/systemd/system/default.target.wants
 ln -s /etc/systemd/system/ansible-initial.service $DIR/etc/systemd/system/multi-user.target.wants/ansible-initial.service
 ln -s /etc/systemd/system/ansible-initial.service $DIR/etc/systemd/system/default.target.wants/ansible-initial.service
}

### Main code

# Set defaults
MEMORY=512
DISKSIZE=10G
VGROUP=`vgdisplay 2>/dev/null | grep VG.Name |awk ' { print $3 ; } '| tail -1`
OVERWRITE=false
DRBD_PRIORITY=10
DIST='bionic'
WORKSTATION=false
REINSTALL=no
FORCE=''
DOM0DIST=$(lsb_release -cs)

# Process options
while getopts "h:s:p:v:oM:Sx:d:rw?" Option ; do
 case $Option in
  '?') usage; exit 1;;
  h) NEWHOST=$OPTARG ;;
  s) DISKSIZE=${OPTARG}G ;;
  p) DRBD_PRIORITY=$OPTARG ;;
  v) VGROUP=$OPTARG ;;
  o) OVERWRITE=true ;;
  M) MEMORY=$OPTARG ;;
  S) SSH='/bin/true ||' ; SCP='/bin/true ||' ; STANDALONE='t' ;;
  x) XM=$OPTARG ;;
  d) DIST=$OPTARG
  if ! [[ "$DIST" == "jessie"  || "$DIST" == "xenial" || "$DIST" == "bionic" || "$DIST" == "focal" ]]; then
    echo Unknown dist $OPTARG ; exit 3
  fi
  ;;
  r) REINSTALL=yes ; FORCE='-F' ;;
  w) WORKSTATION=true ;;
  *) echo Unknown option $Option ; usage; exit 3;;
 esac
done

if [[ "$DIST" == 'focal' && "$DOM0DIST" != 'focal' ]] ; then
 echo Cannot create a focal domU on a non-focal dom0
 exit 2
fi

# Here are some basic checks
if [ -z $NEWHOST ] ; then
 echo No hostname provided
 usage
 exit 1
fi

if [[ "$REINSTALL" == "no" ]] ; then
 # Get MAC address from database
 if [ -z $MACADDR ]; then
     ensuremac $NEWHOST
 fi
 checkuniquemac $MACADDR
fi

# empirically, 64MB is not enough to unpack the initramfs but 96 is
if [ $MEMORY -lt 96 ] ; then
  echo $MEMORY MB of memory is not enough\; try specifying at least 96
fi
ensuredns $NEWHOST
ensurenovm $NEWHOST
SHORTHOST=${NEWHOST/.*/}
warnifnametoolong $SHORTHOST
checkdrbd

if [[ "$REINSTALL" == "no" ]] ; then
  getvlan
fi

# Debug: display all the settings:
echo Hostname $NEWHOST, [DISKSIZE=$DISKSIZE] MACADDR=$MACADDR [MEMORY=$MEMORY] [VGROUP=$VGROUP] VLAN=$VLAN REINSTALL=$REINSTALL
export SHORTHOST NEWHOST DISKSIZE MACADDR MEMORY OVERWRITE REINSTALL

# Check we don't already have a DRBD device of that name unless just reinstalling
if [[ "$REINSTALL" == "no" ]] ; then
 if $DRBDINUSE ; then
  ensurenodrbd $SHORTHOST
 fi
fi

getrootpasswd

if [[ "$REINSTALL" == "no" ]] ; then
 # Create the logical volume(s) unless just reinstalling
 createlv $SHORTHOST $VGROUP $DISKSIZE
 if $DRBDINUSE ; then
  # Create the DRBD device also
  createdrbd $SHORTHOST
 fi
else
 if ! $DRBDINUSE ; then
  LVNAME=$SHORTHOST
 fi
fi

getblockdev

if [[ "$REINSTALL" == "no" ]] ; then
 # Work on the config for the newly-built DomU
 writexenconfig $DIST
fi

# Create root filesystem
mke2fs $FORCE -t ext4 $BLOCKDEV

# Work on the newly-built DomU
bootstrap $BLOCKDEV $DIST
sshkeys /mnt/xen-$SHORTHOST/etc/ssh
setrootpw /mnt/xen-$SHORTHOST
writefstab /mnt/xen-$SHORTHOST
gettyonconsole /mnt/xen-$SHORTHOST
getchemgpgkey /mnt/xen-$SHORTHOST
sourceslist /mnt/xen-$SHORTHOST $DIST
installpackages /mnt/xen-$SHORTHOST
if ($WORKSTATION) ; then
 installworkstationpackages /mnt/xen-$SHORTHOST
 ansibleinitial /mnt/xen-$SHORTHOST
fi
installgrub /mnt/xen-$SHORTHOST
unmountchroot /mnt/xen-$SHORTHOST

# Finalise configuration
if $DRBDINUSE ; then
 if [[ "$REINSTALL" == "no" ]] ; then
  configha
 fi
 sleep 3
 drbdadm secondary $SHORTHOST
fi

# Start the VM 
$XM create /etc/xen/${SHORTHOST}.cfg
