#!/bin/bash
set -e
# To adjust the 'after' statements in DRBD resources

# to bootstrap, each resource file needs a block like:
#
# syncer {
#  # Priority 10
#  after foo;
# }
# 
# The 'foo' is just a placeholder and will be replaced with
# a resource name after you run this script. 
# Multiple resouces can have the same Priority

cd /etc/drbd.d

# Check - does each resource have an 'after' statement? (If not, sed will fail)
FILES=`ls -1 *.res | wc -l`
AFTERS=`grep after *.res | wc -l`
PRIORITIES=`grep Priority *.res | wc -l`
if [ $FILES != $AFTERS ] ; then
 echo Each file should have a single 'after' statement, this is not the case.
 for A in *.res ; do grep -q after $A || echo $A missing after line ; done
 exit 1
fi

if [ $FILES != $PRIORITIES ] ; then
 echo Each file should have a single \'# Priority\' line, this is not the case.
 for A in *.res; do grep -q Priority $A || echo $A missing Priority line; done
 exit 1
fi

# Loop over each resource
PREV=
grep -H Priority *.res | sed s/Priority// | sed 's/:\|#//g' | sort -n -k2 | awk ' { print $1 } ' | while read FILE ; do
 # Remove comment from in front of after
 sed -i "s/#AUTO after/after/" $FILE
 if [ -z $PREV ] ; then
  # Place comment in front of after
  sed -i "s/after/#AUTO after/" $FILE
 else 
  # Put the previous resource name after after
  sed -i "s/after .*/after $PREV;/" $FILE
 fi
 RES=`grep -v '^#' $FILE | grep resource  | awk ' { print $2 } ' | sed 's/"//g'`
 PREV=$RES
done
