#!/bin/bash
#set -x
# to create virtual machines, winbind them, on LVM on DRBD
SSH="ssh twin"
SCP='scp'
XM='xl'
ME=`hostname`
set -e 

### Functions

# Script:
function usage() {
 cat <<EOF
 Usage: $0 -h Hostname (FQDN) [-o][-D -m MACADDR] [-s size/GB] [-v volume_group] [-M memory/MB ] 
 builds a virtual machine. 
 -o over-writes any existing config files if the machine is not already running
 -S standalone ie there is no twin machine
 -D if machine is in the DMZ (i.e. can't look stuff up from pxe), so also
    add -m MACADDR
 -x xm for xm instead of xl
EOF
 exit 1
}


### Functions which ensure the world is ready for this machine

 ##
 # Check whether a named DRBD device exists
 ##
function drbdexists() {
 local SHORTHOST=$1
 ((drbdadm state $SHORTHOST >/dev/null 2>&1) &&
  (! drbdadm state $SHORTHOST 2>&1 | grep -q "no resources defined" )) 
}

 ##
 # Set MAC address from database, bale if not found 
 ##
function ensuremac() {
 local NEWHOST=$1
 if ! $DMZ ; then
  MACADDR=` wget -q -O - http://pxe.ch.cam.ac.uk/fai/getmac.php?hostname=$NEWHOST`
 fi
 if [ -z $MACADDR ] ; then
  echo No MAC address could be found.
  usage
  exit 1
 fi
}

 ##
 # Check we can resolve this hostname (i.e. there's an IP address set)
 # and bale if not.
 ##
function ensuredns() {
 NEWHOST=$1
 # Is hostname in DNS yet?
 if ! getent hosts $NEWHOST >/dev/null 2>&1 ; then
 cat <<EOF
 * Cannot resolve $NEWHOST. Not proceeding further. 
 * (you could always add to /etc/hosts if required)
EOF
 exit 3
fi
}

 ## 
 # Check we haven't already created a DRBD device with this name
 # Bale out if we have
 ## 
function ensurenodrbd() {
 local SHORTHOST=$1
 if drbdexists $SHORTHOST ; then
  cat <<EOF
  * The short name ($SHORTHOST) is already in use, according to DRBD.
  * Stop the drbd, edit /etc/drbd.conf and remove references to it if
  * you're sure that it's not in use.
umount -f `drbdadm sh-dev $SHORTHOST`
drbdadm down $SHORTHOST && rm /etc/drbd.d/$SHORTHOST.res
$SSH drbdadm down $SHORTHOST && $SSH rm /etc/drbd.d/$SHORTHOST.res
EOF
  exit 5
 fi
 if (grep ^resource /etc/drbd.conf | grep -q '[ "]'$SHORTHOST'[ "]' ) ; then
  cat <<EOF
  * $SHORTHOST is still listed in /etc/drbd.conf. You need to remove that before continuing.
EOF
  exit 5
 fi
 if ([ -d /etc/drbd.d/ ] && [ -f /etc/drbd.d/${SHORTHOST}.res ]) ||
    ($SSH [ -d /etc/drbd.d/ ] && $SSH [ -f /etc/drbd.d/${SHORTHOST}.res ]) ; then
  if ($OVERWRITE) ; then
  cat <<EOF
   * Removing configuration at /etc/drbd.d/${SHORTHOST}.res.
EOF
  rm -f /etc/drbd.d/${SHORTHOST}.res 
   $SSH rm -f /etc/drbd.d/${SHORTHOST}.res
  fi;
  if [ -d /etc/drbd.d/ ] && [ -f /etc/drbd.d/${SHORTHOST}.res ] ; then
   cat <<EOF
   * Failed to remove configuration at /etc/drbd.d/${SHORTHOST}.res. 
EOF
   exit 5
  fi
 fi
 
}

 ##
 # Check we haven't already created a DomU with this name, bale if we have
 ##
function ensurenovm() {
 local NEWHOST=$1
 # Does that VM exist?
 if $XM list $NEWHOST >/dev/null 2>&1 ; then
  cat <<EOF
  * Xen is already running a machine of that name. If you want to replace that VM, 
  * stop the virtual machine first, and remove entries from /etc/drbd.conf, /etc/ha.d/haresources
EOF
  exit 4
 fi
}

 ##
 # Over-long names are bad for Windows domains. Check this.
 ##
function nametoolong() {
 local SHORTHOST=$1
 if [ ${#SHORTHOST} -gt 15 ] ; then
  cat <<EOF
  * ${SHORTHOST} is ${#SHORTHOST} characters long. Names longer than 15 characters
  * cannot be joined to the Active Directory.
  * Please try again with a shorter hostname. (Perhaps ${SHORTHOST:1:15}?)
EOF
  exit 2
 fi
}

function getvlan() {
  VLAN=$(wget -O - http://linux-builder.ch.cam.ac.uk/ansible/vlan.php?mac=$MACADDR |head -1) 
  if [ -z $VLAN ]
  then
    echo No VLAN found, using vlan 1
    VLAN=1
  fi
}

### Functions which adjust the Dom0 to handle this machine

 ##
 # To check that we have a few tools which might not be here
 ##
function checkbinaries() {
 debootstrap --version
 pvdisplay --version
}

 ##
 # Boolean function to check whether DRBD is in use
 ##
function checkdrbd() {
 if [ -f /proc/drbd ] && [ "x$STANDALONE" == 'x' ]; then
  DRBDINUSE=true
 else
  DRBDINUSE=false
 fi
}

 ## 
 # Configure heartbeat
 ##
function configha() {
 # Add this machine to heartbeat
 sed 's/'$ME'.*/& xenlive::'$NEWHOST'/' -i /etc/ha.d/haresources
 $SCP /etc/ha.d/haresources root@twin:/etc/ha.d/

 # Copy the config to twin
 $SCP /etc/xen/$NEWHOST root@twin:/etc/xen/$NEWHOST
}
 
 ##
 # Creates a DRBD device, set BLOCKDEV
 ##
function createdrbd() {
 local SHORTHOST=$1
 if [ -d /etc/drbd.d/ ] ; then
  newdrbd $SHORTHOST $VGROUP >>/etc/drbd.d/${SHORTHOST}.res
  $SCP /etc/drbd.d/${SHORTHOST}.res root@twin:/etc/drbd.d/${SHORTHOST}.res
 else 
  cp /etc/drbd.conf /etc/drbd.conf-pre-$SHORTHOST
  $SSH cp /etc/drbd.conf /etc/drbd.conf-pre-$SHORTHOST
  newdrbd $SHORTHOST $VGROUP >>/etc/drbd.conf
  $SCP /etc/drbd.conf root@twin:/etc/drbd.conf
 fi
 # Start the drbd
 yes | drbdadm create-md $SHORTHOST
 yes | $SSH drbdadm create-md $SHORTHOST
 drbdadm up $SHORTHOST
 $SSH drbdadm up $SHORTHOST
 DEVICE=`drbdadm dump $SHORTHOST | grep -A2 $ME | grep device | awk ' { print $2 ; } ' | sed s/\;//`
 drbdsetup $DEVICE primary --overwrite-data-of-peer
 mke2fs -t ext4 $DEVICE
 sleep 6
 drbdadm secondary $SHORTHOST
 drbdadm primary $SHORTHOST
 BLOCKDEV=`drbdadm sh-dev $SHORTHOST`
}

 ##
 # Create logical volume on this node, and on twin IFF drbd is in use 
 ##
function createlv() {
 local SHORTHOST=$1
 local VGROUP=$2
 local DISKSIZE=$3
 ensurenolv $SHORTHOST
 # Create the logical volume
 lvcreate --size $DISKSIZE --name $SHORTHOST $VGROUP
 if $DRBDINUSE; then
  $SSH lvcreate --size $DISKSIZE --name $SHORTHOST $VGROUP
 fi
 # And blank them
 MDSIZE=500000
 SIZE=`lvs --units b /dev/${VGROUP}/${LVNAME} | grep $SHORTHOST | awk ' { print \$4 ; } ' | sed s/B//`
 SKIP=$((SIZE-MDSIZE))
 dd if=/dev/zero of=/dev/${VGROUP}/${LVNAME} bs=1 count=$MDSIZE skip=$SKIP
 dd if=/dev/zero of=/dev/${VGROUP}/${LVNAME} bs=1 count=$MDSIZE seek=$SKIP
 if $DRBDINUSE ; then
  $SSH dd if=/dev/zero of=/dev/${VGROUP}/${LVNAME} bs=1 count=$MDSIZE skip=$SKIP
  $SSH dd if=/dev/zero of=/dev/${VGROUP}/${LVNAME} bs=1 count=$MDSIZE seek=$SKIP
 fi
}

 ##
 # We haven't already got an LV of that name, have we?
 ##
function ensurenolv() {
 local SHORTHOST=$1
 # Does that VG exist?
 # (That grep call is needed - vgdisplay of a missing vg returns 0)
 if ! vgdisplay $VGROUP 2>&1 | grep -q Metadata ; then
  cat <<EOF
  * That volume group does not exist. Please choose from:
EOF
  vgdisplay | grep Name | awk ' { print $3 ; } '
  exit 6
 fi
 # Does that LV exist?
 LVNAME=${SHORTHOST}
 if [ -b /dev/${VGROUP}/${LVNAME} ]; then
  if ($OVERWRITE) ; then
   if (mount | grep -q /dev/${VGROUP}/${LVNAME}) then
set -xv
    if lsof /dev/${VGROUP}/${LVNAME} >/dev/null 2>&1 ; then
     lsof /dev/${VGROUP}/${LVNAME}  | awk ' { print $2 ; } ' | grep -v PID| xargs kill -9 
    fi
    umount -f /mnt/xen/proc || umount -l /mnt/xen/proc || true
    umount -f /mnt/xen/dev || true
    umount -f /dev/${VGROUP}/${LVNAME} || true
   fi
 set -e
   lvremove -f /dev/${VGROUP}/${LVNAME}
   if $DRBDINUSE ; then
    $SSH lvremove -f /dev/${VGROUP}/${LVNAME}
   fi
  else
   cat <<EOF
  * That logical volume already exists. Perhaps you need to remove it with 
  * lvremove /dev/${VGROUP}/${LVNAME}
EOF
   exit 7
  fi
 fi
}

 ##
 # Creates a new DRBD device
 ##
function newdrbd() {
 local NAME=$1
 # //FOO/BAR replaces all FOO with BAR, not just 1st FOO
 LVNAME=${NAME}
 VG=$2
 local VGROUP=$VG
 THISHOST=`hostname`
 TWINHOST=`$SSH hostname`
 LASTDEV=`cat /proc/drbd | grep '[ds:|ro:]' | awk ' { print $1 ; } ' | sed s/:// | sort -n | tail -1`
 THISDEV=drbd$((LASTDEV+1))
 LASTDEV=`$SSH cat /proc/drbd | grep '[ds:|ro:]' | awk ' { print $1 ; } ' | sed s/:// | sort -n | tail -1`
 TWINDEV=drbd$((LASTDEV+1))
 THISIP=`ifconfig eth1 | grep inet.addr | sed s/:/\ /g | awk ' { print $3 ; } '`
 TWINIP=`$SSH ifconfig eth1 | grep inet.addr | sed s/:/\ /g | awk ' { print $3 ; } '`
 # if this is the first drbd need to start port numbers at 7789 or something
 LASTPORT=`drbdadm dump | grep $THISIP | sed s/.*:// | sed s/\;// | sort -n | tail -1`
 if [ -z "$LASTPORT" ] ; then LASTPORT=7788; fi
 THISPORT=$((LASTPORT+1))
 LASTPORT=`$SSH drbdadm dump | grep $TWINIP | sed s/.*:// | sed s/\;// | sort -n | tail -1`
 if [ -z "$LASTPORT" ] ; then TWINPORT=7788; fi
 TWINPORT=$((LASTPORT+1))
 
cat <<EOF
# Automatically generated by $0 on `hostname` at `date`
resource "$NAME" {
EOF
if [ ! -z $LAST ] ; then cat <<EOF
 syncer { after $LAST; }
EOF
fi
cat <<EOF
 on $THISHOST {
  device        /dev/$THISDEV;
  disk          /dev/${VGROUP}/${LVNAME};
  address       $THISIP:$THISPORT;
  meta-disk     internal;
 }
 on $TWINHOST {
  device        /dev/$TWINDEV;
  disk          /dev/${VGROUP}/${LVNAME};
  address       $TWINIP:$TWINPORT;
  meta-disk     internal;
 }
}

EOF
}

 ##
 # Writes config for Xen
 ##
function writexenconfig() {
 if $DRBDINUSE ; then
  DISK="['drbd:$SHORTHOST,xvda1,w']"
 else
  if [ "$XM" eq "xl" ] ; then
   DISK="['$BLOCKDEV,raw,xvda1,w']"
  else
   DISK="['phy:$BLOCKDEV,xvda1,w']"
  fi
 fi
 cat >/etc/xen/$NEWHOST <<EOF
# Automatically created by $0 at `date`

kernel      = '/etc/xen/kernels/xenial/vmlinuz'
ramdisk     = '/etc/xen/kernels/xenial/initrd.img'
extra       = 'clocksource=xen xencons=tty console=hvc0 elevator=noop'
root        = '/dev/xvda1 ro'

memory = '$MEMORY'
 
# Disks
disk = $DISK

# Hostname
name='$NEWHOST'

# Networking - ensure that the MAC address is unique.
vif=['bridge=vlanbr$VLAN, mac=$MACADDR']

on_poweroff = 'destroy'
on_reboot   = 'restart'
on_crash    = 'restart'

vncpasswd = ''

cpus = 'all,^0-1'

EOF
}

 ##
 # Mounts /proc
 ##
function mountproc() {
 local DIR=$1
 mount -o bind /proc $DIR/proc
}

function umountproc() {
 local DIR=$1
 # Can't look for /dev/mapper device if it's drbd!
 lsof | grep /mnt/xen | awk ' { print $2 ; } ' | grep -v PID| xargs kill -9
 umount $DIR/dev || true
 umount $DIR/proc || true
 sleep 2
 umount $DIR ||true
}

### Functions which operate directly in the DomU

 ##
 # Drop to a shell to allow the user to mess around
 ##
function fiddlewithdomu() {
 local DIR=$1
 
 cat <<EOF
Fiddle around in the newly created DomU. Exit this shell and confirm to continue and boot the machine
EOF
 OK=no
 OLDDIR=`pwd`
 while [ ! "$OK" == 'ok' ] ; do
  cd $DIR
  PS1="$SHORTHOST :" bash
  echo Type 'ok' to continue, anything else to go back and edit again
  read OK
 done
 cd $OLDDIR
 echo Attempting to unmount $DIR
 set +e
}

 ##
 # Make the /rsnapshots bit work
 ##
function makersnapshots() {
 local DIR=$1
 mkdir $DIR/rsnapshots
 #chroot $DIR apt-get -y install autofs5 linux-modules-`uname -r`
 #chroot $DIR apt-get -y --force-yes install autofs5 linux-modules-2.6.32-5
 grep -v rsnapshots $DIR/etc/auto.master >/tmp/auto.master.new || true
 cat <<EOF >>/tmp/auto.master.new
/-      /etc/auto.rsnapshots --timeout=60
EOF
 mv /tmp/auto.master.new $DIR/etc/auto.master
 cat <<EOF >/etc/auto.rsnapshots
/rsnapshots        -ro splot.ch.cam.ac.uk:/srv/nfs4/$NEWHOST
EOF
}

 ##
 # Set the root password 
 ##
function setrootpw() {
 local DIR=$1
 while ! chroot $DIR /usr/bin/passwd ; do
  echo Oh, that didn\'t work - try again
 done
}

function gettyonconsole() {
 local DIR=$1
 echo '7:2345:respawn:/sbin/getty 38400 hvc0' >> $DIR/etc/inittab
}

function sourceslist() {
 local DIR=$1
 cat <<EOF > $DIR/etc/apt/sources.list
###### Ubuntu Main Repos
deb http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu/ xenial main restricted universe
deb-src http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu/ xenial main restricted universe

###### Ubuntu Update Repos
deb http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu/ xenial-security main restricted universe
deb http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu/ xenial-updates main restricted universe
deb-src http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu/ xenial-security main restricted universe
deb-src http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu/ xenial-updates main restricted universe
EOF

 cat <<EOF > $DIR/etc/apt/sources.list.d/downloads.list
deb     http://downloads.ch.cam.ac.uk/local-debs xenial ucamchem
EOF
chroot $DIR apt-get update
}

function writefstab() {
 local DIR=$1
 cat <<EOF >> $DIR/etc/fstab
/dev/xvda1 / ext4 errors=remount-ro 0       1
EOF
}

 ##
 # Set some debconf stuff here
 ##
function setdebconf() {
 local DIR=$1
 chroot $DIR debconf-set-selections <<EOF
tzdata  tzdata/Areas    select  Europe
tzdata  tzdata/Zones/Europe     select  London
EOF
# cp -aR /etc/xen-tools/skel/* $DIR
}


 ##
 # Bootstrap the basics of a machine into a given directory
 ##
function bootstrap() {
 local BLOCKDEV=$1
 # Now, prepare the VM and boot it on the network
 # Except we can't PXE boot the damn things.
 echo Bootstrapping a basic OS onto the host
 mkdir -p /mnt/xen
 mount $BLOCKDEV /mnt/xen
 debootstrap --include=udev,openssh-server,less,sudo,vim,rsync xenial /mnt/xen http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu
 cat <<EOF >/mnt/xen/etc/network/interfaces
auto lo
iface lo inet loopback

# Cannot use DHCP in a DomU. No idea why. RFL 04/06/2010
auto eth0
iface eth0 inet static
EOF
 IP=`getent hosts $NEWHOST | awk '{print $1}'`
 if $DMZ ; then
  GW=`echo $IP | sed 's/[0-9][0-9]*$/62/'`
  echo address $IP >> /mnt/xen/etc/network/interfaces
  echo netmask 255.255.255.0 >> /mnt/xen/etc/network/interfaces
  echo gateway $GW >> /mnt/xen/etc/network/interfaces
  cp /etc/resolv.conf /mnt/xen/etc/resolv.conf
 else
  # Add network and resolv.conf settings from the database
  wget -q -O - http://pxe.ch.cam.ac.uk/fai/getnet.php?hostname=$NEWHOST >>/mnt/xen/etc/network/interfaces
  wget -q -O - http://pxe.ch.cam.ac.uk/fai/getresolv.php?hostname=$NEWHOST >/mnt/xen/etc/resolv.conf
 fi

 echo $SHORTHOST >/mnt/xen/etc/hostname
 echo $IP $SHORTHOST $NEWHOST >>/mnt/xen/etc/hosts

}

 ##
 # Put the SSH keys in place
 ##
function sshkeys() {
 local DIR=$1
 # sort out SSH keys - copied from a FAI script
 if ! $DMZ; then
 URL=http://pxe.ch.cam.ac.uk/fai/ssh.php

 wget -O $DIR/ssh_host_rsa_key.pub $URL?file=ssh_host_rsa_key.pub
 wget -O $DIR/ssh_host_rsa_key     $URL?file=ssh_host_rsa_key
 wget -O $DIR/ssh_host_dsa_key.pub $URL?file=ssh_host_dsa_key.pub
 wget -O $DIR/ssh_host_dsa_key     $URL?file=ssh_host_dsa_key
 wget -O $DIR/ssh_host_ecdsa_key     $URL?file=ssh_host_ecdsa_key
 wget -O $DIR/ssh_host_ecdsa_key.pub     $URL?file=ssh_host_ecdsa_key.pub
 wget -O $DIR/ssh_host_ed25519_key     $URL?file=ssh_host_ed25519_key
 wget -O $DIR/ssh_host_ed25519_key.pub     $URL?file=ssh_host_ed25519_key.pub
 chmod 0600 $DIR/ssh_host_rsa_key $DIR/ssh_host_dsa_key $DIR/ssh_host_ecdsa_key $DIR/ssh_host_ed25519_key
 fi

 # Add frank's public key and catherine's
 mkdir -p /mnt/xen/root/.ssh
 cat <<EOF >>/mnt/xen/root/.ssh/authorized_keys
ssh-dss AAAAB3NzaC1kc3MAAACBAO4AsFV/R6Y2hYur/RIB0WdQrEy4yDW5wIO9J2SFRaZXRU+6Pk0KkTAOBklsTXSeH38zrk6FuvN1XIwHJmq+Y0hu9uB5orqqZesPvkwlxxV6QGV/Cw3Jeut8XK2C8DagMgPYLO3xk3zrrXW9S156Tl9+zdY4/QimD/RoiUhFoYBZAAAAFQDv5QpAbt4yvXtMEhywy3lfztcWoQAAAIEA3G2YxkRNMnIY8Rh7O7nfCSDjJycaZGB1pNJaweU/4cbbmPEMYvsSePcL6OmxS4DStVWFyZWmExUVmw1VH1DRQa1zsNS05l8d6qNUnOXQpo8UYkD3ekv6m2YAD9jmkcdzqXtsHB0ZPi5UvQ5KDD/fWcjj5fL00fI4MxG1P/0EqtEAAACBAJmfsKvCFR1R9MnAlct31JyWTAAum11ElVAwh8PH2eQETnnXbwxx0TVsp5ac3vtvsUm4pZbuBnR88PbmfC6mDU+Uz6pXV7N3ujQ1zm15qsbvHUrH5eKQEH+OOSIMc8scWigrsgvAMmkCrRRA6MtPfpGtCZcpRfiUYZ5sqkTkq6a2 rl201@chm
ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAr35dZdJHWAWj3t1ad6L/Z1FvEz3mdhpAAM8jR3qVUM4NtfupRc6Giz9rQswAD6fGQSgWfB+5aug+FYK78YLE1/Y0X6GGSM8NAxS96cGGghYXtwhhVjQS6cJFFAUYCrigXN3MQhBc1Zznye5VJWD0gD8gn/82jpkRJ1txxWO4OL3XrDjnvXwz/JzPoFchkz9GZlpKJ0hXOpxt3rxebPToBXznx1ELT+BzfW0oWhxyLakN9bmpK/tw2CsXgDIWt1o38czh9IEuvQpcrk3d/wzQoz+oWEEvTXU4HUT3MnxsS6O+3dUJkUjqmiSFkQU7BG6Y+r0gOUa8BIKHftKcxOVJ+Q== cen1001@liberator
ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAIEAzAeaAYigQd54f4X+7w36EPTf+njmzyt1wm0bxwz0IujybWuAHg7DwggyRkXc6g4GJKnTBypoO3fwI5nFM8Q6VXv7ja08+YqMtgczCbQn/eq0SJeaJQyJ9LFohpZ/WJ7FwNuJOGTdSKMTpQyB8whGUFCvEJwmlEiaPaOW0jMnWvc= root@chm.ch.cam.ac.uk.
EOF
 chmod 0600 /mnt/xen/root/.ssh/authorized_keys
}

## install hobbit and co
function installhobbit() {

 local DIR=$1
 local NEWHOST=$2
 echo hobbit-client   hobbit-client/HOBBITSERVERS     string  131.111.112.27 >> $DIR/tmp/debconf
 echo xymon-client   hobbit-client/HOBBITSERVERS     string  131.111.112.27  >> $DIR/tmp/debconf
 chroot $DIR debconf-set-selections /tmp/debconf
 chroot $DIR apt-get install -y --force-yes xymon-client hobbit-plugins xymon-chem-domu
 # now fix the /etc.default/hobbit file
 sed -i "s/CLIENTHOSTNAME.*/CLIENTHOSTNAME=\"$NEWHOST\"/" $DIR/etc/default/xymon-client
 chroot $DIR usermod -a -G adm xymon

}

## fix email
function fixmail() {

 local DIR=$1
 echo root: root@ch.cam.ac.uk >> $DIR/etc/aliases
 echo root: root@ch.cam.ac.uk >> $DIR/etc/email-addresses
 echo exim4-config    exim4/dc_local_interfaces       string  127.0.0.1 > $DIR/tmp/debconf
 echo exim4-config    exim4/dc_minimaldns     boolean false >> $DIR/tmp/debconf
 echo exim4-config    exim4/dc_eximconfig_configtype  select  mail sent by smarthost\; no local mail >> $DIR/tmp/debconf
 echo exim4-config    exim4/dc_readhost       string  cam.ac.uk >> $DIR/tmp/debconf
 echo exim4-config    exim4/dc_smarthost      string  ppsw.cam.ac.uk >> $DIR/tmp/debconf
 chroot $DIR debconf-set-selections /tmp/debconf
 chroot $DIR apt-get -y --force-yes install exim4-daemon-light
 echo $NEWHOST > $DIR/etc/mailname

}

### Main code

# Set defaults
MEMORY=128
DISKSIZE=1G
VGROUP=`vgdisplay | grep VG.Name |awk ' { print $3 ; } '| tail -1`
OVERWRITE=false
DMZ=false

# Process options
while getopts "h:s:m:c:p:v:oSDM:x:" Option ; do
 case $Option in
  h) NEWHOST=$OPTARG ;;
  s) DISKSIZE=${OPTARG}G ;;
  v) VGROUP=$OPTARG ;;
  o) OVERWRITE=true ;;
  M) MEMORY=$OPTARG ;;
  S) SSH='/bin/true ||' ; SCP='/bin/true ||' ; STANDALONE='t' ;;
  m) MACADDR=$OPTARG ;;
  D) DMZ=true ;;
  x) XM=$OPTARG ;;
  *) echo Unknown option $Option ; exit 3;;
 esac
done

# Here are some basic checks
if [ -z $NEWHOST ] ; then
 echo No hostname provided
 usage
 exit 1
fi
# Get MAC address from database
if [ -z $MACADDR ]; then
    ensuremac $NEWHOST
fi
# empirically, 64MB is not enough to unpack the initramfs but 96 is
if [ $MEMORY -lt 96 ] ; then
  echo $MEMORY MB of memory is not enough; try specifying at least 96
fi
ensuredns $NEWHOST
ensurenovm $NEWHOST
SHORTHOST=${NEWHOST/.*/}
nametoolong $SHORTHOST
checkdrbd
getvlan

# Debug: display all the settings:
echo Hostname $NEWHOST, [DISKSIZE=$DISKSIZE] MACADDR=$MACADDR [MEMORY=$MEMORY] [VGROUP=$VGROUP] [DMZ=$DMZ] VLAN=$VLAN
export SHORTHOST NEWHOST DISKSIZE MACADDR MEMORY OVERWRITE DMZ

# Check we don't already have a DRBD device of that name
if $DRBDINUSE ; then
 ensurenodrbd $SHORTHOST
fi

# Create the logical volume
createlv $SHORTHOST $VGROUP $DISKSIZE

if $DRBDINUSE ; then
 # Create the DRBD device
 createdrbd $SHORTHOST
else
 # Use the LV device
 BLOCKDEV=/dev/${VGROUP}/${LVNAME}
 mke2fs -t ext4 /dev/${VGROUP}/${LVNAME}
fi

# Work on the config for the newly-build DomU
writexenconfig

# Work on the newly-built DomU
bootstrap $BLOCKDEV
sshkeys /mnt/xen/etc/ssh
setdebconf /mnt/xen
setrootpw /mnt/xen
writefstab /mnt/xen
gettyonconsole /mnt/xen
# some setup seems to be needed for wheezy, not sure why
sourceslist /mnt/xen
set -xv
mountproc /mnt/xen
makersnapshots /mnt/xen
installhobbit /mnt/xen $NEWHOST
fixmail /mnt/xen
fiddlewithdomu /mnt/xen
umountproc /mnt/xen

# Finalise configuration
if $DRBDINUSE ; then
 configha
 sleep 3
 drbdadm secondary $SHORTHOST
fi

# Start the VM - not under HA yet
$XM create /etc/xen/$NEWHOST
