#!/bin/bash
# to create virtual machines, winbind them, on LVM on DRBD
SSH="ssh twin"
ME=`hostname`
set -e 
LIBS=`dirname $0`/new-vm-libs
. $LIBS

# Set defaults
MEMORY=1024
DISKSIZE=2G
VGROUP=`vgdisplay 2>/dev/null | grep VG.Name |awk ' { print $3 ; } '| tail -1`
OVERWRITE=false
DMZ=false

# Process options
while getopts "h:s:m:c:p:v:oSDM:" Option ; do
 case $Option in
  h) NEWHOST=$OPTARG ;;
  s) DISKSIZE=${OPTARG}G ;;
  v) VGROUP=$OPTARG ;;
  o) OVERWRITE=true ;;
  M) MEMORY=$OPTARG ;;
  S) SSH='/bin/true ||' ;;
  m) MACADDR=$OPTARG ;;
  D) DMZ=true ;;
  *) echo Unknown option $Option ; exit 3;;
 esac
done

# Here are some basic checks
if [ -z $NEWHOST ] ; then
 echo No hostname provided
 usage
 exit 1
fi
# Get MAC address from database
if [ -z $MACADDR ]; then
    ensuremac $NEWHOST
fi
ensuredns $NEWHOST
ensurenovm $NEWHOST
SHORTHOST=${NEWHOST/.*/}
nametoolong $SHORTHOST
checkdrbd

# Debug: display all the settings:
echo Hostname $NEWHOST, [DISKSIZE=$DISKSIZE] MACADDR=$MACADDR [MEMORY=$MEMORY] [VGROUP=$VGROUP] [DMZ=$DMZ]
export SHORTHOST NEWHOST DISKSIZE MACADDR MEMORY OVERWRITE DMZ

# Check we don't already have a DRBD device of that name
if $DRBDINUSE ; then
 ensurenodrbd $SHORTHOST
fi

# Create the logical volume
createlv $SHORTHOST $VGROUP $DISKSIZE

if $DRBDINUSE ; then
 # Create the DRBD device
 createdrbd $SHORTHOST
else
 # Use the LV device
 BLOCKDEV=/dev/${VGROUP}/${LVNAME}
 mke2fs -j /dev/${VGROUP}/${LVNAME}
fi

# Work on the config for the newly-build DomU
writexenconfig

# Work on the newly-built DomU
bootstrap $BLOCKDEV
sshkeys /mnt/$SHORTHOST/etc/ssh
setdebconf /mnt/$SHORTHOST
setrootpw /mnt/$SHORTHOST
gettyonconsole /mnt/$SHORTHOST
# some setup seems to be needed for squeeze, not sure why
sourceslist /mnt/$SHORTHOST

#mountproc /mnt/xen # Gets mounted during bootstrap now
makersnapshots /mnt/$SHORTHOST
installhobbit /mnt/$SHORTHOST $NEWHOST
fixmail /mnt/$SHORTHOST
fiddlewithdomu /mnt/$SHORTHOST
umountproc /mnt/$SHORTHOST

# Umount the world
zfs set mountpoint=none $SHORTHOST
zfs set mountpoint=none $SHORTHOST/root
zpool export $SHORTHOST

# Finalise configuration
if $DRBDINUSE ; then
 configha
 sleep 3
 drbdadm secondary $SHORTHOST
fi

# Start the VM - not under HA yet
xl create /etc/xen/$NEWHOST
